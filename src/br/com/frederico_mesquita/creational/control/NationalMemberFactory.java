package br.com.frederico_mesquita.creational.control;

import br.com.frederico_mesquita.creational.entity.IMembership;
import br.com.frederico_mesquita.creational.entity.IMembershipFactory;
import br.com.frederico_mesquita.creational.entity.MonthlylMember;
import br.com.frederico_mesquita.creational.entity.SubscriptionType;

public class NationalMemberFactory implements IMembershipFactory {

	@Override
	public IMembership subscribe(SubscriptionType subscriptionType, String name) {
		IMembership member = null;
		
		switch (subscriptionType) {
		case MONTHLY:
			member = new MonthlylMember();
			break;
		default:
			break;
		}
		
		if(null != member) {
			member.registerMember(name);
		}
		
		return member;
	}

}
