package br.com.frederico_mesquita.creational;

import java.util.ArrayList;
import java.util.List;

import br.com.frederico_mesquita.creational.bondary.FactoryProducer;
import br.com.frederico_mesquita.creational.entity.IMembership;
import br.com.frederico_mesquita.creational.entity.IMembershipFactory;
import br.com.frederico_mesquita.creational.entity.Nationality;
import br.com.frederico_mesquita.creational.entity.SubscriptionType;

public class ManageMembers {
	public static void main(String[] args) {
		List<IMembership> lstMember = new ArrayList<>();
		
		IMembershipFactory internationalFactory = FactoryProducer.getMemberFactory(Nationality.OTHER);
		IMembershipFactory nationalFactory = FactoryProducer.getMemberFactory(Nationality.BRAZILLIAN);
		
		lstMember.add(internationalFactory.subscribe(SubscriptionType.ANNUAL_MEMBER, "Teddy"));
		lstMember.add(internationalFactory.subscribe(SubscriptionType.TEMPORARY_MEMBER, "Robert"));
		lstMember.add(internationalFactory.subscribe(SubscriptionType.LIFETIME, "Suzan"));
		
		lstMember.add(nationalFactory.subscribe(SubscriptionType.MONTHLY, "Gonçalves"));
		
		for(IMembership member : lstMember) {
			member.showMember();
		}
	}
}
