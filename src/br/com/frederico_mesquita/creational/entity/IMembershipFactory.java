package br.com.frederico_mesquita.creational.entity;

public interface IMembershipFactory {
	IMembership subscribe(SubscriptionType subscriptionType, String name);
}
