package br.com.frederico_mesquita.creational.entity;

import java.time.Instant;
import java.util.UUID;

public class AnnualMember extends Membership {

	@Override
	public void registerMember(String param) {
		this.setUserId(UUID.randomUUID().toString());
		this.setUserName(param);
		this.setStartDate(Instant.now());
		this.setDiscountPercentage(new Float(30));
		this.setSubscriptionType(SubscriptionType.ANNUAL_MEMBER);
		this.setEndDate(1);
	}

}
