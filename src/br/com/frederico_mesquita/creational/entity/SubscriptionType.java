package br.com.frederico_mesquita.creational.entity;

public enum SubscriptionType {
	ANNUAL_MEMBER, TEMPORARY_MEMBER, LIFETIME, MONTHLY;
}
