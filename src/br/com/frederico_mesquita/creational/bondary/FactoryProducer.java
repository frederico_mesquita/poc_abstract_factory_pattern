package br.com.frederico_mesquita.creational.bondary;

import br.com.frederico_mesquita.creational.control.InternationalMemberFactory;
import br.com.frederico_mesquita.creational.control.NationalMemberFactory;
import br.com.frederico_mesquita.creational.entity.IMembershipFactory;
import br.com.frederico_mesquita.creational.entity.Nationality;

public class FactoryProducer {
	public static IMembershipFactory getMemberFactory(Nationality nationality) {
		
		IMembershipFactory iMemberFactory = null;
		
		switch (nationality) {
		case BRAZILLIAN:
			iMemberFactory = new NationalMemberFactory();
			break;
		case OTHER:
			iMemberFactory = new InternationalMemberFactory();
			break;
		default:
			break;
		}
		
		return iMemberFactory;
	}
}
